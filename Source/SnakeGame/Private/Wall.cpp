#include "Wall.h"
#include "SnakeBase.h"

AWall::AWall()
{
    PrimaryActorTick.bCanEverTick = false;
}

void AWall::Interact(AActor* Interactor, bool bIsHead)
{
    if (IsValid(Interactor) && bIsHead)
    {
        ASnakeBase* Snake = Cast<ASnakeBase>(Interactor);
        if (Snake)
        {
            Snake->Die();
        }
    }
}
