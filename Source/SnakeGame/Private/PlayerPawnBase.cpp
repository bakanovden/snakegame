#include "PlayerPawnBase.h"
#include <Engine/Classes/Camera/CameraComponent.h>
#include "SnakeBase.h"
#include "Components/InputComponent.h"

APlayerPawnBase::APlayerPawnBase()
{
    PrimaryActorTick.bCanEverTick = true;
    PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
    RootComponent = PawnCamera;
}

void APlayerPawnBase::BeginPlay()
{
    Super::BeginPlay();
    SetActorRotation(FRotator(-90, 0, 0));
    CreateSnakeActor();
}

void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
    Super::SetupPlayerInputComponent(PlayerInputComponent);

    PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVerticalInput);

    PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);
}

void APlayerPawnBase::CreateSnakeActor()
{
    SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());
}

void APlayerPawnBase::HandlePlayerVerticalInput(float value)
{
    if (IsValid(SnakeActor))
    {
        if (value > 0.0f && SnakeActor->LastMovementDirection != EMovementDirection::DOWN)
        {
            SnakeActor->NewMovementDirection = EMovementDirection::UP;
        }
        else if (value < 0.0f && SnakeActor->LastMovementDirection != EMovementDirection::UP)
        {
            SnakeActor->NewMovementDirection = EMovementDirection::DOWN;
        }
    }
}

void APlayerPawnBase::HandlePlayerHorizontalInput(float value)
{
    if (IsValid(SnakeActor))
    {
        if (value > 0.0f && SnakeActor->LastMovementDirection != EMovementDirection::LEFT)
        {
            SnakeActor->NewMovementDirection = EMovementDirection::RIGHT;
        }
        else if (value < 0.0f && SnakeActor->LastMovementDirection != EMovementDirection::RIGHT)
        {
            SnakeActor->NewMovementDirection = EMovementDirection::LEFT;
        }
    }
}
