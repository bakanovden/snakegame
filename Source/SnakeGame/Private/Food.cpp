#include "Food.h"
#include "SnakeBase.h"

AFood::AFood()
{
    PrimaryActorTick.bCanEverTick = false;
}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
    ASnakeBase* Snake = Cast<ASnakeBase>(Interactor);
    if (IsValid(Snake) && bIsHead)
    {
        Snake->AddSnakeElement();
        Destroy();
    }
}
