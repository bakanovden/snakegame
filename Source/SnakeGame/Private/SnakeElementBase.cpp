#include "SnakeElementBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "SnakeBase.h"
#include "Kismet/KismetMathLibrary.h"

ASnakeElementBase::ASnakeElementBase()
{
    MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
    MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
    MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
    SetRootComponent(MeshComponent);
}

void ASnakeElementBase::SetFirstElementType_Implementation()
{
    Head = true;
    MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ASnakeElementBase::OnComponentOverlap);
}

void ASnakeElementBase::SetSnakeOwner(ASnakeBase* NewSnakeOwner)
{
    SnakeOwner = NewSnakeOwner;
}

void ASnakeElementBase::Interact(AActor* Interactor, bool bIsHead)
{
    if (Interactor->IsA<ASnakeBase>())
    {
        Interactor->Destroy();
    }
}

void ASnakeElementBase::OnComponentOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
    if (IsValid(SnakeOwner))
    {
        SnakeOwner->SnakeElementOverlap(this, OtherActor);
    }
}

void ASnakeElementBase::ToggleCollision()
{
    if (MeshComponent->GetCollisionEnabled() == ECollisionEnabled::NoCollision)
    {
        MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
    }
    else
    {
        MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
    }
}
