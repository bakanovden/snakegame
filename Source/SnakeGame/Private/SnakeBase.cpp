#include "SnakeBase.h"
#include "SnakeElementBase.h"

ASnakeBase::ASnakeBase()
{
    PrimaryActorTick.bCanEverTick = true;
}

void ASnakeBase::BeginPlay()
{
    Super::BeginPlay();
    SetActorTickInterval(MovementFrequency);
    AddSnakeElement(StartCountElements);

    if (SnakeElements.Num() > 0)
    {
        SnakeElements[0]->SetFirstElementType();
        SnakeElements[0]->SetActorHiddenInGame(false);
    }
}

void ASnakeBase::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    Move();
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
    for (int i = 0; i < ElementsNum; i++)
    {
        FVector NewLocation(SnakeElements.Num() * ElementSize, 0.0f, 0.0f);
        FTransform NewTransform(NewLocation);
        ASnakeElementBase* NewSnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
        NewSnakeElement->SetSnakeOwner(this);
        NewSnakeElement->SetActorHiddenInGame(true);
        SnakeElements.Add(NewSnakeElement);
    }
}

void ASnakeBase::Move()
{
    FVector MovementVector(ForceInitToZero);
    switch (NewMovementDirection)
    {
    case EMovementDirection::UP:
        MovementVector.X += ElementSize;
        break;
    case EMovementDirection::DOWN:
        MovementVector.X -= ElementSize;
        break;
    case EMovementDirection::LEFT:
        MovementVector.Y -= ElementSize;
        break;
    case EMovementDirection::RIGHT:
        MovementVector.Y += ElementSize;
        break;
    default:
        break;
    }

    SnakeElements[0]->ToggleCollision();

    for (int i = SnakeElements.Num() - 1; i > 0; --i)
    {
        ASnakeElementBase* CurrentElement = SnakeElements[i];
        CurrentElement->SetActorHiddenInGame(false);
        ASnakeElementBase* PrevElement = SnakeElements[i - 1];
        FVector PrevLocation = PrevElement->GetActorLocation();
        CurrentElement->SetActorLocation(PrevLocation);
    }

    SnakeElements[0]->AddActorWorldOffset(MovementVector);
    SnakeElements[0]->ToggleCollision();
    LastMovementDirection = NewMovementDirection;
}

void ASnakeBase::Die()
{
    Destroy();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
    if (!IsValid(OverlappedElement))
    {
        return;
    }

    if (IInteractable* InteractableInterface = Cast<IInteractable>(Other))
    {
        InteractableInterface->Interact(this, OverlappedElement->Head);
    }
}
