#pragma once

#include <CoreMinimal.h>
#include <GameFramework/GameModeBase.h>

#include "SnakeGameGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME_API ASnakeGameGameModeBase : public AGameModeBase
{
    GENERATED_BODY()
};
