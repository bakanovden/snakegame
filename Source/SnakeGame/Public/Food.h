
#pragma once

#include <CoreMinimal.h>
#include <GameFramework/Actor.h>

#include "Interactable.h"
#include "Food.generated.h"

UCLASS()
class SNAKEGAME_API AFood : public AActor, public IInteractable
{
    GENERATED_BODY()

public:
    AFood();

    virtual void Interact(AActor* Interactor, bool bIsHead) override;
};
