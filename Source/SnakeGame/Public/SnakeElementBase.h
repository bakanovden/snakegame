#pragma once

#include <CoreMinimal.h>
#include <GameFramework/Actor.h>

#include "Interactable.h"
#include "SnakeElementBase.generated.h"

class UStaticMeshComponent;
class ASnakeBase;

UCLASS()
class SNAKEGAME_API ASnakeElementBase : public AActor, public IInteractable
{
    GENERATED_BODY()

public:
    ASnakeElementBase();

    UFUNCTION(BlueprintNativeEvent)
    void SetFirstElementType();

    void SetSnakeOwner(ASnakeBase* NewSnakeOwner);

    virtual void Interact(AActor* Interactor, bool bIsHead) override;

    UFUNCTION()
    void OnComponentOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

    UFUNCTION()
    void ToggleCollision();

public:
    UPROPERTY()
    bool Head = false;

protected:
    UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, Category="Components")
    UStaticMeshComponent* MeshComponent;

    UPROPERTY()
    ASnakeBase* SnakeOwner;
};
