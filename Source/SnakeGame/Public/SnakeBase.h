#pragma once

#include <CoreMinimal.h>
#include <GameFramework/Actor.h>

#include "SnakeBase.generated.h"

class ASnakeElementBase;

UENUM()
enum class EMovementDirection
{
    UP,
    DOWN,
    RIGHT,
    LEFT
};

UCLASS()
class SNAKEGAME_API ASnakeBase : public AActor
{
    GENERATED_BODY()

public:
    ASnakeBase();

    virtual void BeginPlay() override;

    virtual void Tick(float DeltaTime) override;

    void AddSnakeElement(int ElementsNum = 1);

    void Move();

    void Die();

    UFUNCTION()
    void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other);

public:
    UPROPERTY()
    EMovementDirection LastMovementDirection = EMovementDirection::DOWN;

    UPROPERTY()
    EMovementDirection NewMovementDirection = EMovementDirection::DOWN;

protected:
    UPROPERTY(EditDefaultsOnly, Category="Settings")
    TSubclassOf<ASnakeElementBase> SnakeElementClass;

    UPROPERTY(EditDefaultsOnly, Category="Settings")
    int32 StartCountElements = 4;

    UPROPERTY()
    TArray<ASnakeElementBase*> SnakeElements;

    UPROPERTY(EditDefaultsOnly, Category="Settings")
    float ElementSize = 100.0f;

    UPROPERTY(EditDefaultsOnly, Category="Settings")
    float MovementFrequency = 0.5f;

    UPROPERTY()
    bool IsMoved;
};
